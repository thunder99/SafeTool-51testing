#!/usr/bin/env python
# -*- coding:utf-8 -*-
from madb.adb.adbpackage import Adb
from madb.adb.settings import command,c_note
from madb.tools import plist
from madb.tools.printer import plus,info,warn
from threading import Timer
import os
import _thread as thread

adbObj = Adb()

def show_command(r="1-10"):
    b,e = r.split("-")
    rList = [str(i) for i in range(int(b),int(e)+1)]
    plus("adb命令如下:")
    for i in rList:
        info("["+i+"] "+" ".join(command[i]) + ": " +c_note[i])

def show_command_rule(r="shell"):
    for k,v in command.items():
        if r in v:
            info("["+k+"] "+" ".join(v) + ": " + c_note[k])

def adb_execute(order:str):
    adbObj.execute(order)

def adb_cust_execute(order:list):
    adbObj.customize_execute(order)

def show_pid_info(pid:str,grep="Manager"):
    order = "adb shell ps -t -p "
    order += pid + " | grep " + grep
    orderlist = order.split()
    adbObj.customize_execute(orderlist)

def find_spec_pid(grep="com.mwr.dz"):
    order = "adb shell ps"
    order += " | grep " + grep
    orderlist = order.split()
    adbObj.customize_execute(orderlist)


def find_spec_logcat(grep="51testing"):
    order = "adb shell logcat | grep"
    order += " " + grep
    orderlist = order.split()
    adbObj.customize_execute(orderlist,False,3)

def adb_shell_cmd():
    wtSetUp = "wt.exe adb shell"
    cmdSetUp = "cmd /c start adb shell"
    if os.system(wtSetUp) != 0:
        os.system(cmdSetUp)
def start_adb_shell():
    thread.start_new_thread(adb_shell_cmd,())
