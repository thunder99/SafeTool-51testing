#!/usr/bin/env python
# -*- coding:utf-8 -*-

module = {
    "MADB": "封装并执行adb命令,目前收集60个adb命令,可扩展且可执行自定义命令.",
    "MHOOK": "封装Hook框架,可扩展,目前集成进程劫持检测脚本.",
    "MDROZER": "用py3封装并重写drozer客户端核心代码,目的是灵活扩展或重写安全测试脚本.",
    "MSAFE": "可用于集成安全测试脚本."
}

madb = {}

mhook = {
    "AndroGoat":{
        "f_51testingPoc": "用于教学演示进程检测."
    }
}

mdrozer = {
    "get_devices_info":"测试环境信息.",
    "start_server": "接收drozer代理数据,转换序列化数据版本不一致问题."
}

msafe = {
    "attacksurface":"检测四大组件攻击面.",
    "permissions":"检测apk权限."
}