#!/usr/bin/env python
# -*- coding:utf-8 -*-
from sys import modules,stdin
from mhook.frame.fridaframe import fhook
from mhook.module.android import fhook_poc,onmessage,textview_message_handler
from mhook.tools import check,set,setting
import base64

def check_envir():
    check.envir()

def show_list_devices():
    set.list_devices()

def set_connect_devices(params):
    if params == "mumu":
        set.connect_mumu()

def set_frida_forward():
    set.set_frida_forward()

def show_forward():
    set.forward_list()

def show_frida_ps():
    set.frida_ps()

def find_app():
    set.find_app()

def poc_51testingTeach():
    devicesid = setting.mobile["mumu"].split()[2]
    attach = setting.tapp['teching']['name']
    pocCode = fhook_poc['AndroGoat']
    fh = fhook(devicesid,attach)
    fh.start_poc(pocCode)
    
def poc_myappSumCalc():
    devicesid = setting.mobile["mumu"].split()[2]
    attach = setting.tapp['myapp']['name']
    pocCode = fhook_poc['myapp']['sumcalc']
    fh = fhook(devicesid,attach)
    fh.new_start_poc(pocCode,onmessage)

def poc_myappSameFunc():
    devicesid = setting.mobile["mumu"].split()[2]
    attach = setting.tapp['myapp']['name']
    pocCode = fhook_poc['myapp']['samefunc']
    fh = fhook(devicesid,attach)
    fh.new_start_poc(pocCode,onmessage)

def poc_myappSecretFunc():
    devicesid = setting.mobile["mumu"].split()[2]
    attach = setting.tapp['myapp']['name']
    pocCode = fhook_poc['myapp']['secretfunc']
    fh = fhook(devicesid,attach)
    fh.new_start_poc(pocCode,onmessage)
def poc_myappRpcSecret():
    try:
        devicesid = setting.mobile["mumu"].split()[2]
        attach = setting.tapp['myapp']['name']
        pocCode = fhook_poc['myapp']['secretrpc']
        fh = fhook(devicesid,attach)
        pocScript = fh.call_rpc_func(pocCode,onmessage)
        pocScript.exports.hooksecret()
        stdin.read()
    except KeyboardInterrupt:
        print("hook程序停止!")
    except Exception as e:
        print(e)
        print("fhook错误!")

def base64_encode(coderStr):
    try:
        str_coder = base64.b64encode(coderStr.encode('utf-8'))
        return str_coder.decode('utf-8')
    except Exception as e:
        print(e)
        return "编码错误!"

def poc_myappTextView():
    try:
        devicesid = setting.mobile["mumu"].split()[2]
        attach = setting.tapp['myapp']['name']
        pocCode = fhook_poc['myapp']['textview']
        fh = fhook(devicesid,attach)
        pocScript = fh.call_rpc_func(pocCode,textview_message_handler)
        data = "admin:123456"
        f_data = base64_encode(data)
        pocScript.exports.hooktextview()
        pocScript.post({"fix_data":f_data})
        stdin.read()
    except KeyboardInterrupt:
        print("hook程序停止!")
    except Exception as e:
        print(e)
        print("fhook错误!")

