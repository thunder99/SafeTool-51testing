#!/usr/bin/env python
# -*- coding:utf-8 -*-

from msafe.module.attacksurface import AttackSurface
from msafe.module.permissions import Permissions

def detect_activity_attacksurface(apkPath):
    test = AttackSurface(apkPath)
    test.enum_activity_attack_surface()

def detect_receiver_attacksurface(apkPath):
    test = AttackSurface(apkPath)
    test.enum_receiver_attack_surface()

def detect_provider_attacksurface(apkPath):
    test = AttackSurface(apkPath)
    test.enum_provider_attack_surface()

def detect_service_attacksurface(apkPath):
    test = AttackSurface(apkPath)
    test.enum_service_attack_surface()

def detect_permissions(apkPath):
    test = Permissions(apkPath)
    test.list_permissions()