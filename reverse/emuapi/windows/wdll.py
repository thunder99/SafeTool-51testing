#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import pefile
PATH = os.path.dirname(os.path.realpath(__file__))
DLLPATH = os.path.join(PATH,"dll")


def is_dll_exists(dllName):
    dlls = []
    for _,_,fname in os.walk(DLLPATH):
        dlls = fname
    if dllName in dlls:
        return True
    return False

def dll_dict(dllName):
    impDes = {}
    if is_dll_exists(dllName):
        dll = DLLPATH + os.sep + dllName
        pe = pefile.PE(dll,fast_load=True)
        pe.parse_data_directories()
        dllAddr = []
        for exp in pe.DIRECTORY_ENTRY_EXPORT.symbols:
            if exp.name:
                impDes[pe.OPTIONAL_HEADER.ImageBase +exp.address] = exp.name.decode()
            else:
                continue
        for k in impDes.keys():
            dllAddr.append(k)
        dllAddr.sort()
        return impDes,dllAddr
    else:
        return False
