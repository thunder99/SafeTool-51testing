#!/usr/bin/env python
# -*- coding:utf-8 -*-
from lib.utils import printer
from lib import settings

def print_ip(debugObj:object):
    debugObj.dump_ip(context = debugObj.context)
def print_ax(debugObj:object):
    debugObj.dump_ax(context=debugObj.context)
def print_bx(debugObj:object):
    debugObj.dump_bx(context=debugObj.context)
def print_cx(debugObj:object):
    debugObj.dump_cx(context=debugObj.context)
def print_dx(debugObj:object):
    debugObj.dump_dx(context=debugObj.context)
def print_di(debugObj:object):
    debugObj.dump_di(context=debugObj.context)
def print_si(debugObj:object):
    debugObj.dump_si(context=debugObj.context)
def print_bp(debugObj:object):
    debugObj.dump_bp(context=debugObj.context)
def print_sp(debugObj:object):
    debugObj.dump_sp(context=debugObj.context)
def print_stack(debugObj:object):
    stack_context = debugObj.stack_dump_context(context = debugObj.context)
    printer.warn("<----------------------------------",flag="")
    printer.test(stack_context,flag="[栈内数据]\n")
    printer.warn("<----------------------------------",flag="")
def rmem_address(debugObj:object,address):
    try:
        iaddr = int(address,16)
        c = debugObj.read_process_memory_ex(iaddr,8)
        printer.test("[内存数据]0x%08x (%10d) -> %s" % (iaddr,iaddr,c))
    except Exception as e:
        printer.warn("内存数据读取失败!")
    
def debug_stop(debugObj:object):
    debugObj.stop()

def debug_order(order:str,debugObj:object):
    if order == "rip":
        print_ip(debugObj)
    elif order == "rax":
        print_ax(debugObj)
    elif order == "rbx":
        print_bx(debugObj)
    elif order == "rcx":
        print_cx(debugObj)
    elif order == "rdx":
        print_dx(debugObj)
    elif order == "rdi":
        print_di(debugObj)
    elif order == "rsi":
        print_si(debugObj)
    elif order == "rbp":
        print_bp(debugObj)
    elif order == "rsp":
        print_sp(debugObj)
    elif order == "stack":
        print_stack(debugObj)
    elif order.find("rmem") >=0:
        addr = order.split(" ")[1]
        rmem_address(debugObj,addr)
    elif order == "q":
        debug_stop(debugObj)
    
