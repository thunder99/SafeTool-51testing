#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os

#路径配置
ROOT_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)),"../")#项目路径
LIB_PATH = os.path.join(ROOT_PATH,"lib")
LIB_HANDLE_PATH = os.path.join(LIB_PATH,"handle")

#动态调试处理模块配置
BP_HANDLE = {
    'breakpoint.py':"[默认]断点事件处理模块"
}
SS_HANDLE = {
    'singlestep.py':"[默认]单步调试事件处理模块",
    'sslow.py':"单步调试事件慢速处理模块",
    'sdisasm.py':"单步调试事件反汇编处理模块",
    "sinject.py":"修改程序运行逻辑处理模块"
}

Debug_ORDER = {
    "rip":"",
    "rax":"",
    "rbx":"",
    "rcx":"",
    "rdx":"",
    "rdi":"",
    "rsi":"",
    "rbp":"",
    "rsp":"",
    "stack":"",
    "flags":"",
    "rmem":"",
    "wmem":""
}