#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
#根路径
PROJECT_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)),"../../")
#字典路径
DICT_PATH = os.path.join(PROJECT_PATH,"dict")

#集成平台路径
EXREND_PATH =  os.path.join(PROJECT_PATH,"extend")

#插件路径
PLUGIN_PATH =  os.path.join(EXREND_PATH,"plugin")

#分割文件路径
SPLITF_PATH = os.path.join(PLUGIN_PATH,"sfile")