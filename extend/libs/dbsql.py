#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sqlite3
import os
import traceback
import json

#数据库文件绝对路径
PROJECT_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)),"../../")
DB_PATH = os.path.join(PROJECT_PATH,"db")
DB_FILE_PATH = DB_PATH + "\\mitmex.db"
def get_conn(path):
    conn = sqlite3.connect(path)
    if os.path.exists(path) and os.path.isfile(path):
        return conn
    else:
        conn = None
        return sqlite3.connect(':memory:')
def get_cursor(conn):
    if conn is not None:
        return conn.cursor()
    else:
        return get_conn('').cursor()
def close_all(conn, cu):
    '''关闭数据库游标对象和数据库连接对象'''
    try:
        if cu is not None:
            cu.close()
    finally:
        if conn is not None:
            conn.close()
def mitmex_get_cur(path):
    conn = get_conn(path)
    cu = get_cursor(conn)
    return conn,cu
###############################################################
####            DDL操作     START
###############################################################
def mitmex_del_data_all(table):
    try:
        sql = '''delete from '''+table
        conn,cu = mitmex_get_cur(DB_FILE_PATH)
        cu.execute(sql)
        conn.commit()
        close_all(conn,cu)
    except Exception as e:
        print(traceback.print_exc())
        close_all(conn,cu)
#查询全部数据
def mitmex_select_all(table):
    try:
        data_dict = {}
        result = []
        sql = '''select * from '''+table
        conn,cu = mitmex_get_cur(DB_FILE_PATH)
        cu.execute(sql)
        r = cu.fetchall()
        if table == "mitmhttp":
            if len(r)>0:
                for e in range(len(r)):
                    data_dict['id'] = r[e][0]
                    data_dict['method'] = r[e][1]
                    data_dict['url'] = r[e][2]
                    data_dict['headers'] = eval(r[e][3])
                    data_dict['params'] = r[e][4]
                    data_dict['create_time']  = r[e][5]
                    data_dict['system_name'] = r[e][6]
                    data_dict['is_request'] = r[e][7]
                    data_dict['is_response'] = r[e][8]
                    result.append(data_dict)
                    data_dict = {}         
        close_all(conn,cu)
        return result
    except Exception as e:
        print(traceback.print_exc())
        close_all(conn,cu)
#获得最近的头信息
def mitmex_get_headers_by_system_name(table,systemName):
    rows = mitmex_select_max_data(table,"system_name",systemName,"headers")
    result = {}
    if table == 'httpmultipart':
        if len(rows)>0:
            for r in rows:
                result = eval(r[0])
    return result
#获得最近的参数值
def mitmex_get_params_by_system_name(table,sysName):
    rows = mitmex_select_max_data(table,"system_name",sysName,"params")
    result = ''
    if table == 'httpmultipart':
        if len(rows)>0:
            for r in rows:
                result = r[0]
    return result 
#获得最近的url
def mitmex_get_url_by_system_name(table,sysName):
    rows = mitmex_select_max_data(table,"system_name",sysName,"url")
    result = ''
    if table == 'httpmultipart':
        if len(rows)>0:
            for r in rows:
                result = r[0]
    return result  
#最大值
def mitmex_select_max_data(table,field,value,maxField):
    sql = '''select '''+maxField+''' from '''+table+''' where '''+field+''' = ? order by id desc limit 1'''
    data = (value,)
    conn,cu = mitmex_get_cur(DB_FILE_PATH)
    cu.execute(sql,data)
    r = cu.fetchall()
    close_all(conn,cu)
    return r