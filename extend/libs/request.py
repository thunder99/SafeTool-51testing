#!/usr/bin/env python
# -*- coding:utf-8 -*-


import asyncio
import async_timeout
import libs.printer as printer
try:
    import aiohttp
except ImportError as e:
    printer.warn("请使用pip安装aiohttp=3.7.4")
class Request(object):
    """
    @des
    封装异步HTTP请求,并返回请求内容
    """
    def __init__(self,url,headers={},data="",verb="GET",timeout=3,word = "",sign=False):
        self._url = url
        self._headers = headers
        self._verb = verb
        self._timeout = timeout
        self._data = data
        self._word = word
        self._sign = sign
    
    async def send(self):
        async with aiohttp.ClientSession(headers=self._headers,conn_timeout=self._timeout) as session:
            #请求方法
            verbs = {
                "GET": session.get,
                "POST": session.post,
                "HEAD": session.head,
                "OPTIONS": session.options,
                "PUT": session.put
            }
            args = {}
            if self._verb == "GET":
                pass
            else:
                if len(self._data) > 0:
                    args["data"] = self._data
                    if self._verb not in ["POST","PUT"]:
                        self._verb = "POST"
            with async_timeout.timeout(10):
                async with verbs[self._verb](self._url,**args) as resp:
                    return {
                                "response": resp,
                                "text": await resp.text(),
                                "isOrign": self._sign
                            }
            return "error"

