#!/usr/bin/env python
# -*- coding:utf-8 -*-
#列表去空值
def list_remove_null(strList:list):
    while '' in strList:
        strList.remove('')
    return strList
#返回app
def get_app_name(path:list):
    cleanPath = list_remove_null(path)
    return cleanPath[0]

