#!/usr/bin/env python
# -*- coding:utf-8 -*-

from lib.utils.pyh import *
from lib.utils.settings import *
from lib.utils.printer import *
from os import sep

class HtmlReport:
    def __init__(self,report:dict):
        self.reportData = report
        self.report = PyH(self.reportData['name'])
        self.report.addCSS(REPORT_CSSJS + sep + "bootstrap.min.css")

    def set_html_head(self):
        self.container = self.report << body(id='Body', cl='bg-warning') << div(id="container",cl="container")
        Headrow = self.container << div(id="Headrow", cl="row")
        Headrow << h1(id="HeadH1",align="center") << strong(self.reportData['title'],id="HeadTxt")
        Headrow << br()
    
    def set_html_summary(self):
        Totalrow = self.container << div(id="Totalrow", cl="Totalrow") << div(cl="jumbotron")
        summaryrow = Totalrow << div(id="summaryrow", cl="row")
        summaryrow << div(cl="col-xs-12 col-md-3") << p(role="presentation") << span() <<small("开始时间: ") << span(self.reportData['startTime'], cl="label label-default")
        modrow = Totalrow << div(id="modrow", cl="row")
        execmode = ','.join(self.reportData['execmod'])
        modrow << div(id="mod", cl="col-xs-12 col-md-3") << p(role="presentation") << span() << small("执行模块: ") << span(execmode, cl="label label-default")
        countrow = Totalrow << div(id="countrow", cl="row")
        success = self.reportData['successTotal']
        fail = self.reportData['failTotal']
        total = int(success) + int(fail)
        countrow << div(id="total", cl="col-xs-12 col-md-3") << p(role="presentation") << span() << small("检测项总计: ") << span(total, cl="label label-default")
        countrow << div(id="success", cl="col-xs-12 col-md-3") << p(role="presentation") << span() << small("检测项成功数: ") << span(success, cl="label label-danger")
        countrow << div(id="fail", cl="col-xs-12 col-md-3") << p(role="presentation") << span() << small("检测项失败数: ") << span(fail, cl="label label-warning")

    def set_html_content(self):
        Plans = self.container << div(id="plans", cl="row")
        plans_title = "执行情况"
        PlansTitle = Plans << div(id="plans-title", cl="panel panel-primary") << div(cl="panel-heading") << strong() << center(plans_title, cl="text-uppercase")
        for i in range(len(self.reportData['execmod'])):
            error = "失败"
            success = '成功'
            plan =  Plans << div(id="plan" + str(i+1), cl="col-xs-12 col-md-12") << table(cl="table table-striped")
            modName = self.reportData['execmod'][i]
            title =  modName+ ' [' + plugs_info[modName] + ']'
            plan << center() << caption(title)
            theadlist = ['检测项','执行结果','载荷']
            theads = plan << thead()
            theads << tr() << th(theadlist[0]) + th(theadlist[1]) + th(theadlist[2])
            tbodys = plan << tbody()
            for k,v in self.reportData[modName].items():
                item = k
                result = p(success,cl="label label-danger") if v else error
                payload = v if v else '空'
                tbodys << tr() << td(item) + td(result) + td(payload)
    def generate_report(self):
        self.set_html_head()
        self.set_html_summary()
        self.set_html_content()
        filename = REPORT_HTML + sep + self.reportData['fileName'] + ".html"
        self.report.printOut(filename)
        info("生成测试报告成功!")
        info(filename)
    




