#!/usr/bin/env python 
# -*- coding:utf-8 -*-

from os import path
from re import search,I
from queue import Queue
from threading import Thread
from lib.utils.check import *
from lib.utils.printer import *
from lib.utils.readfile import *
from lib.request.request import *
from lib.utils.settings import BACKUPDIR_PATH,T_MAX

global_isNothing = True
global_result = []
global_cListing = False
def listing(resp):
    """
    检测列表,只要匹配到1个正则就返回真
    """
    _ = False
    _ |= search(r'\<title\>Index of \/', resp, I) is not None
    _ |= search(r'\<a href\=\"?C\=N\;O\=D"\>Name\<\/a\>', resp, I) is not None
    _ |= search(r'\<A HREF\=\"?M\=A\"\>Last modified\<\/A\>', resp, I) is not None
    _ |= search(r'Last modified\<\/a\>', resp, I) is not None
    _ |= search(r'Parent Directory\<\/a\>', resp, I) is not None
    _ |= search(r'\<TITLE\>Folder Listing.', resp, I) is not None
    _ |= search(r'\<table summary\=\"Directory Listing', resp, I) is not None
    _ |= search(r'\"\>[To Parent Directory]\<\/a\>\<br\>\<br\>', resp, I) is not None
    _ |= search(r'Parent Directory', resp, I) is not None
    return _

class commondir(Request):
    """
    检测敏感目录
    """
    get = "GET"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
    
    def check(self):
        info("检测敏感目录...")
        queue = Queue(T_MAX)
        for _ in range(T_MAX):
            thread = ThreadBrute(self.url,queue,self)
            thread.daemon = True
            thread.start()
        for path in readfile(BACKUPDIR_PATH):
            queue.put(path.decode("utf-8"))
        queue.join()

class ThreadBrute(Thread):
    get = "GET"
    def __init__(self,target,queue:Queue,request:Request):
        Thread.__init__(self)
        self.queue = queue
        self.target = target
        self.request = request
    
    def check(self):
        global global_isNothing
        global global_result
        global global_cListing
        while True:
            try:
                path = self.queue.get()
                splitUrl = SplitUrl(self.target)
                netlocUrl = splitUrl.netloc
                url = Cpath(netlocUrl,path)
                more("检测载荷:{}".format(url))
                req = self.request.Send(url=url,method=self.get)
                if req.code == 200:
                    if CEndUrl(req.url) == url:
                        global_result.append(req.url)
                        global_isNothing = False
                        if listing(req.content):
                            global_cListing = True
                self.queue.task_done()
            except Exception as e:
                pass
    def run(self):
        self.check()

def run(kwargs,url,data):
    result = {
        "commondir":None
    }
    scan = commondir(kwargs,url,data)
    scan.check()
    if global_isNothing:
        info_nothing()
    else:
        if global_result:
            for r in global_result:
                plus('疑似找到敏感目录: {}'.format(r))
                if not result['commondir']:
                    result['commondir'] = r
                else:
                    result['commondir'] += ' ,' + r
            if global_cListing:
                plus('返回内容存在敏感信息')
                if not result['commondir']:
                    result['commondir'] = "返回内容存在敏感信息"
                else:
                    result['commondir'] += ' ,' + "返回内容存在敏感信息"
    return result