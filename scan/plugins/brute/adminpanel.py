#!/usr/bin/env python 
# -*- coding:utf-8 -*-

from os import path
from queue import Queue
from threading import Thread
from lib.utils.check import *
from lib.utils.printer import *
from lib.request.request import *
from lib.utils.readfile import *
from lib.utils.settings import T_MAX,ADMINPANEL_PATH

global_isNothing = True
global_result = []
class adminpanel(Request):
    """
    猜解可能的管理面板
    """
    get = "GET"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
    
    def check(self):
        info("蛮力猜解管理后台...")
        queue = Queue(T_MAX)
        for _ in range(T_MAX):
            thread = ThreadBrute(self.url,queue,self)
            thread.daemon = True#守护线程
            thread.start()
        for path in readfile(ADMINPANEL_PATH):
            queue.put(path.decode("utf-8"))
        queue.join()

class ThreadBrute(Thread):
    get = "GET"
    def __init__(self,target,queue:Queue,request:Request):
        Thread.__init__(self)
        self.queue = queue
        self.target = target
        self.request = request

    def check(self):
        global global_isNothing
        global global_result
        while True:
            try:
                splitUrl = SplitUrl(self.target)
                netlocUrl = splitUrl.netloc
                url = Cpath(netlocUrl,self.queue.get())
                more("检测载荷:{}".format(url))
                req = self.request.Send(url=url,method=self.get)
                if req.code == 200:
                    if CEndUrl(req.url) == url:
                        global_result.append(req.url)
                        global_isNothing = False
                self.queue.task_done()
            except Exception as e:
                pass
            except AttributeError as e:
                pass
            except TypeError as e:
                pass
    def run(self):
        self.check()
def run(kwargs,url,data):
    result = {
        'adminpanel':None
    }
    scan = adminpanel(kwargs,url,data)
    scan.check()
    if global_isNothing:
        info_nothing()
    else:
        if global_result:
            for r in global_result:
                plus('疑似找到后台管理页面: {}'.format(r))
                if not result['adminpanel']:
                    result['adminpanel'] = r
                else:
                    result['adminpanel'] += " ," + r
    return result