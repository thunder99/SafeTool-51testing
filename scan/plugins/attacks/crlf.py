#!/usr/bin/env python
# -*- coding:utf-8 -*-
from re import search,I
from lib.utils.params import *
from lib.utils.printer import *
from lib.utils.check import *
from lib.utils.rand import r_string
from lib.request.request import *
from lib.utils.payload import crlfp

class crlf(Request):
    """
    crlf注入 回车换行注入 响应拆分 会话固定 xss
    """
    get = "GET"
    post = "POST"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'crlf':None
        }
    def check(self):
        info("检测 CRLF 注入[回车换行注入]...")
        URL = None
        DATA = None
        PAYLOAD = None
        isNothing = True
        for payload in crlfp():
            random_string = r_string(20)
            payload = payload.replace('=injection',random_string)
            more("检测载荷:{}".format(Cpath(self.url,'/%s'%payload)))
            req = self.Send(Cpath(self.url,'/%s'%payload),method=self.get)
            if 'Set-Cookie' in req.headers.keys():
                if search(random_string,req.headers['Set-Cookie'],I):
                    plus('疑似存在CRLF注入')
                    more('URL[地址]: {}'.format(req.url))
                    more('PAYLOAD[有效载荷]: {}'.format(payload))
                    break
            if self.data:
                addPayload = preplace(self.url,payload,self.data)
                for data in addPayload.run():
                    more("检测载荷:{},{}".format(self.url,data))
                    req = self.Send(url=self.url,method=self.post,data = data)
                    if 'Set-Cookie' in req.headers.keys():
                        if search(random_string,req.headers['Set-Cookie'],I):
                            URL = req.url
                            DATA = data
                            PAYLOAD = payload
                            break
            else:
                urls = preplace(self.url,payload,None)
                for url in urls.run():
                    more("检测载荷:{}".format(url))
                    req = self.Send(url = url,method=self.get)
                    if 'Set-Cookie' in req.headers.keys():
                        if search(random_string,req.headers['Set-Cookie'],I):
                            URL = url
                            PAYLOAD = payload
                            break
            if URL and PAYLOAD:
                if DATA != None:
                    plus("疑似CRLF注入漏洞被找到:")
                    more("URL[地址]: {}".format(URL))
                    more("POST DATA[数据]: {}".format(DATA))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['crlf'] = PAYLOAD
                    isNothing = False
                elif DATA == None:
                    plus("疑似CRLF注入漏洞被找到:")
                    more("URL[地址]: {}".format(URL))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    self.result['crlf'] = PAYLOAD
                    isNothing = False
                break
        if isNothing:
            info_nothing()
        return self.result
def run(kwargs,url,data):
    result = {}
    scan = crlf(kwargs,url,data)
    result = scan.check()
    return result
