#!/usr/bin/env python
# -*- coding:utf-8 -*-
from lib.utils.params import *
from lib.utils.printer import *
from lib.request.request import *
from lib.utils.payload import bsql

class blindsqli(Request):
    """
    SQL 盲注

    """
    get = "GET"
    post = "POST"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'blindsqli':None
        }
    def check(self):
        info("检查SQL盲注漏洞...")
        URL = None
        DATA = None
        CONTENT = None
        PAYLOAD = None
        isNothing = True
        #first
        if self.data:
            req = self.Send(url = self.url,
                method=self.post,
                data=self.data)
            CONTENT = req.content
        else:
            req = self.Send(url=self.url,method=self.get)
            CONTENT = req.content
        #second
        for payload in bsql():
            #post
            if self.data:
                addPayload = padd(self.url,payload,self.data)
                for data in addPayload.run():
                    more("检测载荷:{},{}".format(self.url,data))
                    req = self.Send(url=self.url,
                        method=self.post,
                        data=data)
                    if len(req.content) != len(CONTENT):
                        URL = req.url
                        DATA = data
                        PAYLOAD = payload
                        break
            #get
            else:
                urls = padd(self.url,payload,None)
                for url in urls.run():
                    more("检测载荷:{}".format(url))
                    req = self.Send(url=url,method=self.get)
                    if len(req.content) != len(CONTENT):
                        URL = url
                        PAYLOAD = payload
                        break
            if URL and PAYLOAD:
                if DATA != None:
                    plus("在以下位置找到了可能的注入点:")
                    more("URL[地址]: {}".format(URL))
                    more("POST DATA[参数]: {}".format(DATA))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    warn("检测逻辑比较简单,建议调用sqlmapApi详细探测")
                    self.result['blindsqli'] = PAYLOAD
                    isNothing = False
                elif DATA == None:
                    plus("在以下位置找到了可能的注入点:")
                    more("URL[地址]: {}".format(URL))
                    more("PAYLOAD[有效载荷]: {}".format(PAYLOAD))
                    warn("检测逻辑比较简单,建议调用sqlmapApi详细探测")
                    isNothing = False
                    self.result['blindsqli'] = PAYLOAD
                break
        if isNothing:
            info_nothing()
        return self.result
def run(kwargs,url,data):
    result = {}
    scan = blindsqli(kwargs,url,data)
    result = scan.check()
    return result