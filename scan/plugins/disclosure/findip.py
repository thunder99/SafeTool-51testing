#!/usr/bin/env python 
# -*- coding:utf-8 -*-
import os
import sys
from lib.request.request import *
from lib.utils.printer import *
from lib.parser.parse import *
from lib.utils.settings import *

class findip(Request):
    """
    检测敏感信息 IP
    """
    get = "GET"
    def __init__(self,kwargs,url,data):
        Request.__init__(self,kwargs)
        self.url = url
        self.data = data
        self.result = {
            'findip':None
        }
    def check(self):
        info("检测敏感信息[IP]...")
        req = self.Send(url=self.url,method=self.get)
        more("检测载荷:{},{}".format(self.url,REG_IP))
        self.pIp(req.content)
        return self.result

    def pIp(self,content):
        _list_ = parse(content).getip()
        isNothing = True
        if _list_ != None or _list_ != []:
            if len(_list_) >= 2:
                plus('ip地址被找到: %s'%(str(_list_).split('[')[1].split(']')[0]))
                self.result['findip'] = str(_list_).split('[')[1].split(']')[0]
                isNothing = False
            elif len(_list_) == 1:
                plus('ip地址被找到: %s'%_list_[0])
                self.result['findip'] = _list_[0]
                isNothing = False
        if isNothing:
            info_nothing()
def run(kwargs,url,data):
    result = {}
    scan = findip(kwargs,url,data)
    result = scan.check()
    return result