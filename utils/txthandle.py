#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os

def writeTxt(filename,content):
    try:
        if not os.path.exists(filename):
            with open(filename,"w",encoding="utf-8",errors="ignore") as file_to_write:
                file_to_write.write(content+"\n")
        else:
            with open(filename,"a+",encoding="utf-8",errors="ignore") as file_to_write:
                file_to_write.write(content+"\n")
            file_to_write.close()
    except Exception as e:
        print(traceback.print_exc())
        file_to_write.close()