#!/usr/bin/env python
# -*- coding:utf-8 -*-

import datetime,time

def get_multipart_fn_ftime():
    fname = datetime.datetime.now().strftime("%m%d%H%M%S")
    return fname

def get_multipart_fn_timestamp():
    fname = datetime.datetime.now().timestamp()
    return str(int(fname))